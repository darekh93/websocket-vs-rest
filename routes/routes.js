var randomstring = require('randomstring'); 

var appRouter = function (app) {

  app.get("/", function(req, res) {
    res.render('index');
  });

  app.get("/rest", function(req, res) {
    res.render('rest')
  });

  app.get("/restSizeData", function(req, res) {
    res.render('restSizeData')
  });

  app.get("/websocket", function(req, res) {
    res.render('websocket')
  });

  app.get("/websocketSizeData", function(req, res) {
    res.render('websocketSizeData')
  });

  let test = 0;
  app.get("/api/get", function(req, res) {
    res.status(200).send('' + test);
    test ++;
  });

  app.put("/api/resetCounter", function(req, res) {
    test = 0;
    res.status(200);
  });

  app.post("/api/getSizeData", function(req, res) {
    let data = randomstring.generate({
      length: req.body.size,
      charset: 'alphabetic'
    });
    res.status(200).send({value: data});
  });
}

module.exports = appRouter;