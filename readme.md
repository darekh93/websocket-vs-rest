# Instalacja

  - Należy zainstalować w systemi Node.js minimum w wersji 8, oraz NPM w wersji minimum 6. Instalując Node z oficjalnej strony, zostanie zainstalowane automatycznie NPM. Node można pobrać z https://nodejs.org/en/
  - Po sklonowaniu repozytorium lub pobraniu plików, należy wejść do katalogu projektu i uruchomić polecenie:
  

```sh
$ npm install
```

  - Po udanej instalacji należy wpisać polecenie odpalające serwer: 
  
```sh
$ npm run start
```

  - Po odpaleniu serwera aplikacja dostępna będzie pod adresem http://localhost:8000/