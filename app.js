var path = require("path")
var express = require("express");
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var bodyParser = require("body-parser");
var routes = require("./routes/routes.js");
var randomstring = require('randomstring');
var twig = require('twig');

var port = process.env.PORT || 8000
server.listen(port, function() {
  console.log("app running on port.", server.address().port);
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', __dirname + '/public');
app.set('view engine', 'twig');

routes(app);

let counter = 0;
io.on('connection', function (socket) {
  socket.on('disconnecting', (reason) => {
    console.log('DISCONNECTED')
  });
  socket.on('getCounter', function(callback) {
    counter++;    
    callback(null, counter);
  });
  socket.on('endTest', function(callback) {
    counter = 0;
  })
  socket.on ('dataSizeRequest', function (msg) {
    let data = randomstring.generate({
      length: msg.size,
      charset: 'alphabetic'
    });
  io.sockets.emit ('dataSize', data);
  });
});
